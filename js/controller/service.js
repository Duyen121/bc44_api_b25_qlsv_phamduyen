var BASE_URL = `https://643d66f36afd66da6af605f5.mockapi.io/student`;

var studService = {
  get: function (dataKey) {
    return axios({
      url: `${BASE_URL}${dataKey}`,
      method: "GET",
    });
  },

  delete: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },

  create: function (data) {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: data,
    });
  },

  update: function (id, data) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: data,
    });
  },
};
